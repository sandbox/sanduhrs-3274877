CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Troubleshooting
  * FAQ
  * Maintainers

INTRODUCTION
------------

The CKEditor 5 Mentions module allows you to configure mention functionality
for CKEditor 5. It adds the official CKEditor 5 Mentions plugin to the editor
and allows you to configure any Drupal entity and bundle to be mentionable.

Default preconfigured mention types include:

  * A @mention style mention type e.g., @user
    The @mention style mention type allows users with access to the text format
    where CKEditor 5 and the Mentions plugin is enabled to mention site users.

  * A #hashtag style mention type e.g., #tag
    The #hashtag style mention type allows users with access to the text format
    where CKEditor 5 and the Mentions plugin is enabled to mention terms from
    the Tags vocabulary included in the standard install profile.

  * A +select style mention type e.g., +article
    The +select style mention type allows users with access to the text format
    where CKEditor 5 and the Mentions plugin is enabled to select articles from
    the Article node type included in the standard install profile.

Similar projects

  * Mentions
    The Mentions module offers Twitter like functionality, recording all
    references to a user's username.
    https://www.drupal.org/project/mentions

  * CKEditor Mentions
    This module will provide mentioning users in any CKEditor field, like we
    all known from systems Slack, Jira or Facebook.
    https://www.drupal.org/project/ckeditor_mentions

Further Information

  * For a full description of the module, visit the project page:
    https://www.drupal.org/project/ckeditor5_mentions

  * To submit bug reports and feature suggestions, or track changes:
    https://www.drupal.org/project/issues/ckeditor5_mentions

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module. Visit
    https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

  * Configure the user permissions in Administration » People » Permissions:

    - Administer mention types

      Users with this permission are allowed to create new or edit existing
      mention types.

  * Customize the Text format settings in Administration » Configuration » Text formats and editors:

    - CKEditor5 plugin settings

      Change the Text editor to CKEditor 5 and add the desired Mention type in
      the format's CKEditor5 plugin settings.

  * Add new or change existing Mention types Administration » Mention Types

    - Add / Edit / Delete mention type
      To add, edit or delete a mention type.

MAINTAINERS
-----------

Current maintainers:
 * Stefan Auditor (sanduhrs) - https://www.drupal.org/u/sanduhrs
