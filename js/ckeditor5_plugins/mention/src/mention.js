import { Plugin } from 'ckeditor5/src/core';
import MentionEditing, { _toMentionAttribute } from './mentionediting';
import MentionUI from './mentionui';
import '../theme/mention.css';

/**
 * The mention plugin.
 */
export default class Mention extends Plugin {

	toMentionAttribute( viewElement, data ) {
		return _toMentionAttribute( viewElement, data );
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'Mention';
	}

	/**
	 * @inheritDoc
	 */
	static get requires() {
		return [ MentionEditing, MentionUI ];
	}
}
