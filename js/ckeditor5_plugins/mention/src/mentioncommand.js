import { Command } from 'ckeditor5/src/core';
import { CKEditorError, toMap } from 'ckeditor5/src/utils';
import { _addMentionAttributes } from './mentionediting';

/**
 * The mention command.
 */
export default class MentionCommand extends Command {

  /**
   * @inheritDoc
   */
  refresh() {
    const model = this.editor.model;
    const doc = model.document;
    this.isEnabled = model.schema.checkAttributeInSelection(doc.selection, 'mention');
  }

  /**
   * Executes the command.
   */
  execute(options) {
    const model = this.editor.model;
    const document = model.document;
    const selection = document.selection;
    const mentionData = typeof options.mention == 'string' ? { id: options.mention } : options.mention;
    const mentionID = mentionData.id;
    const range = options.range || selection.getFirstRange();
    const mentionText = options.text || mentionID;
    const mention = _addMentionAttributes({ _text: mentionText, id: mentionID }, mentionData);

    if (options.marker.length != 1) {
      // The marker must be a single character.
      throw new CKEditorError(
        'mentioncommand-incorrect-marker',
        this
      );
    }

    if (mentionID.charAt(0) != options.marker) {
      /**
       * The feed item ID must start with the marker character.
       */
      throw new CKEditorError(
        'mentioncommand-incorrect-id',
        this
      );
    }

    model.change(writer => {
      const currentAttributes = toMap(selection.getAttributes());
      const attributesWithMention = new Map(currentAttributes.entries());

      attributesWithMention.set('mention', mention);

      model.insertContent(writer.createText(mentionText, attributesWithMention), range);
      model.insertContent(writer.createText(' ', currentAttributes), range.start.getShiftedBy(mentionText.length));
    });
  }
}
