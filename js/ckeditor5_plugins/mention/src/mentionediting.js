import { Plugin } from 'ckeditor5/src/core';
import { uid } from 'ckeditor5/src/utils';
import MentionCommand from './mentioncommand';

/**
 * The mention editing feature.
 */
export default class MentionEditing extends Plugin {

  /**
   * @inheritDoc
   */
  static get pluginName() {
    return 'MentionEditing';
  }

  /**
   * @inheritDoc
   */
  init() {
    const editor = this.editor;
    const model = editor.model;

    // Allow the mention attribute on all text nodes.
    model.schema.extend('$text', { allowAttributes: 'mention' });

    editor.conversion.for('upcast').elementToAttribute({
      view: {
        name: 'a',
        key: 'data-mention',
        classes: 'mention',
        attributes: {
          href: true,
          'data-entity-type': true,
          'data-entity-uuid': true,
          'data-mention-type': true,
        }
      },
      model: {
        key: 'mention',
        value: viewItem => {
          const mentionAttribute = editor.plugins.get('Mention').toMentionAttribute(viewItem, {
            link: viewItem.getAttribute('href'),
            entityType: viewItem.getAttribute('data-entity-type'),
            entityUuid: viewItem.getAttribute('data-entity-uuid'),
            mentionType: viewItem.getAttribute('data-mention-type'),
          });
          return mentionAttribute;
        }
      },
      converterPriority: 'high'
    });

    editor.conversion.for('downcast').attributeToElement({
      model: 'mention',
      view: (modelAttributeValue, { writer }) => {
        if (!modelAttributeValue) {
          return;
        }

        return writer.createAttributeElement('a', {
          class: 'mention',
          'data-entity-type': modelAttributeValue.entityType,
          'data-entity-uuid': modelAttributeValue.entityUuid,
          'data-mention': modelAttributeValue.id,
          'data-mention-type': modelAttributeValue.mentionType,
          'href': modelAttributeValue.link
        }, {
          priority: 20,
          id: modelAttributeValue.uid
        });
      },
      converterPriority: 'high'
    });

    editor.commands.add('mention', new MentionCommand(editor));
  }
}

/**
 * Add mention attributes.
 */
export function _addMentionAttributes(baseMentionData, data) {
  return Object.assign({ uid: uid() }, baseMentionData, data || {});
}

/**
 * To mention attribute.
 */
export function _toMentionAttribute(viewElementOrMention, data) {
  const dataMention = viewElementOrMention.getAttribute('data-mention');
  const textNode = viewElementOrMention.getChild(0);

  if (!textNode) {
    return;
  }

  const baseMentionData = {
    id: dataMention,
    _text: textNode.data
  };

  return _addMentionAttributes(baseMentionData, data);
}

export function getFeedItems(query) {
  console.log('WHATEVER!');
}
