<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_mentions\Entity;

use Drupal\ckeditor5_mentions\MentionTypeInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the mentions type entity type.
 *
 * @ConfigEntityType(
 *   id = "mention_type",
 *   label = @Translation("Mention Type"),
 *   label_collection = @Translation("Mention Types"),
 *   label_singular = @Translation("mention type"),
 *   label_plural = @Translation("mention types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count mention type",
 *     plural = "@count mention types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\ckeditor5_mentions\MentionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ckeditor5_mentions\Form\MentionTypeForm",
 *       "edit" = "Drupal\ckeditor5_mentions\Form\MentionTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "mention_type",
 *   admin_permission = "administer mention_type",
 *   links = {
 *     "collection" = "/admin/structure/mention-type",
 *     "add-form" = "/admin/structure/mention-type/add",
 *     "edit-form" = "/admin/structure/mention-type/{mention_type}",
 *     "delete-form" = "/admin/structure/mention-type/{mention_type}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "marker",
 *     "minimum_characters",
 *     "entity_type",
 *     "entity_bundle",
 *     "feed_callable",
 *     "feed_items"
 *   }
 * )
 */
class MentionType extends ConfigEntityBase implements MentionTypeInterface {

  /**
   * The mention type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The mention type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The mention_type description.
   *
   * @var string
   */
  protected $description;

  /**
   * The mention_type marker.
   *
   * @var string
   */
  protected $marker;

  /**
   * The mention_type minimum characters.
   *
   * @var string
   */
  protected $minimum_characters;

  /**
   * The mention_type entity type.
   *
   * @var string
   */
  protected $entity_type;

  /**
   * The mention_type entity bundle.
   *
   * @var string
   */
  protected $entity_bundle;

  /**
   * The mention_type feed callable.
   *
   * @var string
   */
  protected $feed_callable;

  /**
   * The mention_type feed items.
   *
   * @var string[]
   */
  protected $feed_items;

}
