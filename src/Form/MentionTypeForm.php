<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_mentions\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Mentions Type form.
 *
 * @property \Drupal\ckeditor5_mentions\MentionTypeInterface $entity
 */
class MentionTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the mention type.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\ckeditor5_mentions\Entity\MentionType::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the mention type.'),
    ];
    $form['marker'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Marker'),
      '#default_value' => $this->entity->get('marker'),
      '#description' => $this->t('Marker of the mentions type e.g., <em>@</em> or <em>#</em>'),
    ];
    $form['minimum_characters'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum characters'),
      '#default_value' => $this->entity->get('minimum_characters'),
      '#description' => $this->t('The minimum count of characters to type before autocompletion.'),
    ];
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#options' => static::getEntityTypes(),
      '#default_value' => $this->entity->get('entity_type'),
      '#ajax' => [
        'callback' => [$this, 'reloadBundles'],
        'event' => 'change',
        'wrapper' => 'entity-bundle-wrapper',
      ],
      '#required' => TRUE,
    ];
    $form['entity_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity bundle'),
      '#default_value' => $this->entity->get('entity_bundle'),
      '#prefix' => '<div id="entity-bundle-wrapper">',
      '#suffix' => '</div>',
      '#options' => static::getEntityTypeBundles($this->entity->get('entity_type') ?? ''),
      '#states' => [
        'visible' => [
          'select[name="entity_type"]' => ['!value' => ''],
        ],
      ],
    ];
    $form['feed_items'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Feed Items'),
      '#default_value' => implode("\r\n", (array) $this->entity->get('feed_items')),
      '#description' => $this->t('Additional feed items, one per line, to be added to the feed e.g., <em>@all</em>, <em>@team</em> or <em>@channel</em>'),
    ];
    // @todo Make this functional.
    // @see \Drupal\ckeditor5_mentions\Plugin\CKEditor5Plugin::getDynamicPluginConfig
    $form['feed_callable'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Feed Callable'),
      '#default_value' => $this->entity->get('feed_callable'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Massage submitted form values.
    $form_state->setValue('feed_items', explode("\r\n", $form_state->getValue('feed_items')));
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new mention type %label.', $message_args)
      : $this->t('Updated mention type %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public static function reloadBundles(array $form, FormStateInterface $form_state) {
    return $form['entity_bundle'];
  }

  /**
   * Get entity types.
   */
  public static function getEntityTypes() {
    $definitions = \Drupal::entityTypeManager()->getDefinitions();
    $items = [];
    foreach ($definitions as $name => $definition) {
      if (!$definition->getLinkTemplate('canonical')) {
        continue;
      }
      $items[$name] = (string) $definition->getLabel();
    }
    asort($items);
    return $items;
  }

  /**
   * Get entity type bundles.
   */
  public static function getEntityTypeBundles(string $entity_type = '') {
    $bundles = [];
    if (!$entity_type) {
      return $bundles;
    }

    $info = \Drupal::service('entity_type.bundle.info')
      ->getBundleInfo($entity_type);
    foreach ($info as $key => $details) {
      $bundles[$key] = (string) $details['label'];
    }
    return $bundles;
  }

}
