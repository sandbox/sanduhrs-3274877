<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_mentions\Mention;

use Drupal\ckeditor5_mentions\Entity\MentionType;

/**
 * Abstract Mention feed base class.
 */
abstract class MentionFeedBase implements MentionFeedInterface {

  /**
   * The mention type object.
   *
   * @var \Drupal\ckeditor5_mentions\Entity\MentionType
   */
  protected MentionType $mentionType;

  /**
   * The constructor.
   *
   * @param \Drupal\ckeditor5_mentions\Entity\MentionType $mention_type
   *   The mention type object.
   */
  public function __construct(MentionType $mention_type) {
    $this->mentionType = $mention_type;
  }

}
