<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_mentions\Mention;

/**
 * Entity mention feed class.
 */
class MentionFeedEntity extends MentionFeedBase {

  /**
   * {@inheritdoc}
   */
  public function getFeedItems(string $query = ''):array {
    $storage = \Drupal::entityTypeManager()
      ->getStorage($this->mentionType->get('entity_type'));

    $entity_query = $storage->getQuery()->accessCheck(TRUE);
    if ($storage->getEntityType()->getKey('published')) {
      // This is our best guess.
      $entity_query->condition(
        $storage->getEntityType()->getKey('published'),
        TRUE
      );
    }

    if ($this->mentionType->get('entity_bundle')
      && $storage->getEntityType()->getKey('bundle')) {
      $entity_query->condition(
        $storage->getEntityType()->getKey('bundle'),
        $this->mentionType->get('entity_bundle')
      );
    }

    if (!empty($query)
      && $this->mentionType->get('entity_type') === 'user') {
      $or = $entity_query->orConditionGroup()
        ->condition('name', $query, 'CONTAINS')
        ->condition('mail', $query, 'CONTAINS');
      $entity_query->condition($or);
    }
    elseif (!empty($query)
      && $storage->getEntityType()->getKey('label')) {
      $entity_query->condition(
        $storage->getEntityType()->getKey('label'),
        $query,
        'CONTAINS'
      );
    }

    $entity_ids = (array) $entity_query->execute();
    $entities = $storage->loadMultiple($entity_ids);

    $feed_items = [];
    foreach ($entities as $entity) {
      if ($entity->id() == 0) {
        continue;
      }
      $feed_items[] = new MentionFeedItem($this->mentionType, $entity);
    }
    return $feed_items;
  }

}
