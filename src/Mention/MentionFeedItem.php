<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_mentions\Mention;

use Drupal\ckeditor5_mentions\MentionTypeInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Mention feed item.
 */
class MentionFeedItem implements MentionFeedItemInterface {

  /**
   * The mention id.
   *
   * @var string
   */
  public string $id;

  /**
   * The mention type.
   *
   * @var string
   */
  public string $mentionType;

  /**
   * The mention entity type.
   *
   * @var string
   */
  public string $entityType;

  /**
   * The mention entity uuid.
   *
   * @var string
   */
  public $entityUuid;

  /**
   * The mention entity URL.
   *
   * @var string
   */
  public $link;

  /**
   * The constructor.
   *
   * @param \Drupal\ckeditor5_mentions\MentionTypeInterface $mention_type
   *   The mention type object.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The mention type object.
   */
  public function __construct(MentionTypeInterface $mention_type, EntityInterface $entity) {
    $this->id = $mention_type->get('marker') . $entity->label();
    $this->mentionType = $mention_type->get('id');
    $this->entityType = $entity->getEntityType()->id();
    $this->entityUuid = $entity->uuid();
    $this->link = $entity->toUrl()->toString();
  }

}
