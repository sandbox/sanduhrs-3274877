<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_mentions;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a mentions type entity type.
 */
interface MentionTypeInterface extends ConfigEntityInterface {

}
