<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_mentions\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\ckeditor5_mentions\Entity\MentionType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\editor\EditorInterface;

/**
 * CKEditor 5 Mention plugin.
 *
 * @internal
 *   Plugin classes are internal.
 */
class Mention extends CKEditor5PluginDefault implements CKEditor5PluginConfigurableInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * The default configuration for this plugin.
   *
   * @var string[][]
   */
  const DEFAULT_CONFIGURATION = [
    'dropdown_limit' => 10,
    'commit_keys' => [13, 32],
    'mention_types_enabled' => [],
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return static::DEFAULT_CONFIGURATION;
  }

  /**
   * {@inheritdoc}
   *
   * Form for choosing which heading tags are available.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [];
    /** @var \Drupal\ckeditor5_mentions\Entity\MentionType[] $mention_types */
    $mention_types = MentionType::loadMultiple();
    foreach ($mention_types as $mention_type) {
      $options[$mention_type->id()] = $this->t('@label: @description', [
        '@label' => $mention_type->label(),
        '@description' => $mention_type->get('description'),
      ]);
    }
    $form['mention_types_enabled'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled mention types'),
      '#options' => $options,
      '#multiple' => TRUE,
      '#default_value' => $this->configuration['mention_types_enabled'],
    ];
    if (\Drupal::currentUser()->hasPermission('administer filters')) {
      $form['mention_types_enabled']['#description'] = $this->t('You might want to add new or configure existing mention types at the <a href="@url">Mention types</a> configuration page.', [
        '@url' => Url::fromRoute('entity.mention_type.collection')->toString(),
      ]);
    }

    $form['dropdown_limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dropdown limit'),
      '#description' => $this->t('How many items to be shown on the dropdown list.'),
      '#size' => 2,
      '#default_value' => $this->configuration['dropdown_limit'],
    ];
    $form['commit_keys'] = [
      '#type' => 'select',
      '#options' => array_combine(static::DEFAULT_CONFIGURATION['commit_keys'], static::DEFAULT_CONFIGURATION['commit_keys']),
      '#multiple' => TRUE,
      '#attributes' => ['hidden' => 'hidden'],
      '#default_value' => $this->configuration['commit_keys'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['dropdown_limit'] = $form_state->getValue('dropdown_limit');
    $this->configuration['mention_types_enabled'] = $form_state->getValue('mention_types_enabled');
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    $dynamic_plugin_config = $static_plugin_config;
    $dynamic_plugin_config['mention']['dropdownLimit'] = $this->configuration['dropdown_limit'];

    $mention_types = MentionType::loadMultiple(
      $this->configuration['mention_types_enabled']
    );
    foreach ($mention_types as $mention_type) {
      $default_class = 'Drupal\ckeditor5_mentions\Mention\MentionFeedEntity';
      $reflection = new \ReflectionClass($mention_type->get('feed_callable') ?: $default_class);
      $mention_feed = $reflection->newInstanceArgs([$mention_type]);
      $feed_items = $mention_feed->getFeedItems();
      $dynamic_plugin_config['mention']['feeds'][] = [
        'marker' => $mention_type->get('marker'),
        'minimumCharacters' => $mention_type->get('minimum_characters'),
        // @todo Change this to a js callback returning a promise, instead of a
        //   fixed list if items.
        // @see https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#providing-the-feed
        'feed' => array_merge(array_filter((array) $mention_type->get('feed_items')), $feed_items),
      ];
    }
    return $dynamic_plugin_config;
  }

}
